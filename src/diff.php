<?php

namespace makeandship\common;

class Diff
{
    const DIFF_LEFT  = "diff_left";
    const DIFF_RIGHT = "diff_right";

    const INTERSECT_LEFT  = "intersect_left";
    const INTERSECT_RIGHT = "intersect_right";

    /**
     * Diff two arrays using their keys
     */
    public static function diff($left, $right)
    {
        $results = array(
            self::DIFF_LEFT  => array(),
            self::DIFF_RIGHT => array(),
        );

        if (is_array($left) && is_array($right)) {
            $left_keys  = array_keys($left);
            $right_keys = array_keys($right);

            $left_diff  = array_diff($left_keys, $right_keys);
            $right_diff = array_diff($right_keys, $left_keys);

            $missing_left = array();
            foreach ($left_diff as $left_email) {
                $missing_left[$left_email] = $left[$left_email];
            }

            $missing_right = array();
            foreach ($right_diff as $right_email) {
                $missing_right[$right_email] = $right[$right_email];
            }

            $results[self::DIFF_LEFT]  = $missing_left;
            $results[self::DIFF_RIGHT] = $missing_right;
        }

        return $results;
    }

    /**
     * Intersect two arrays using their keys
     */
    public static function intersect($left, $right)
    {
        $results = array(
            self::INTERSECT_LEFT  => array(),
            self::INTERSECT_RIGHT => array(),
        );

        if ($left && $right && is_array($left) && is_array($right)) {
            $left_keys  = array_keys($left);
            $right_keys = array_keys($right);

            $intersect = array_intersect($left_keys, $right_keys);

            $intersect_left = array();
            foreach ($intersect as $left_email) {
                $intersect_left[$left_email] = $left[$left_email];
            }

            $intersect_right = array();
            foreach ($intersect as $right_email) {
                $intersect_right[$right_email] = $right[$right_email];
            }

            $results[self::INTERSECT_LEFT]  = $intersect_left;
            $results[self::INTERSECT_RIGHT] = $intersect_right;
        }

        return $results;
    }
}

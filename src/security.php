<?php

namespace makeandship\common;

class Security
{
    const ADMINISTRATOR_ROLE = 'administrator';

    // can't be instantiated externally
    protected function __construct()
    {
    }
    // no clone
    protected function __clone()
    {
    }

    public static function initialize()
    {
    }

    public static function is_admin($user)
    {
        return Security::has_role($user, Security::ADMINISTRATOR_ROLE);
    }

    public static function has_role($user, $role)
    {
        if ($user && $role) {
            return in_array($role, (array) $user->roles);
        }
        return false;
    }
}

Security::initialize();

<?php
namespace makeandship\common;

class Util
{
    /**
     * Retrieve the value from an array item, or object attribute
     * returning null if the attribute is missing or the value is null
     *
     * @param array the array or object
     * @param attribute the name of the attribute to extract
     * @return the value or the attribute or null if missing
     */
    public static function safely_get_attribute($array, $attribute)
    {
        if (is_array($array)) {
            if (isset($array) && isset($attribute) && $array && $attribute) {
                if (array_key_exists($attribute, $array)) {
                    return $array[$attribute];
                }
            }
        } elseif (is_object($array)) {
            if (isset($array) && isset($attribute) && $array && $attribute) {
                if (property_exists($array, $attribute)) {
                    return $array->{$attribute};
                }
            }
        }
        return null;
    }

    /**
     * Log for a class and a message
     * @param class name
     * @param message
     */
    public static function log($clazz, $message)
    {
        if (defined('WP_DEBUG_LOG') && WP_DEBUG_LOG) {
            error_log($clazz . ": " . print_r($message, true));
        }
    }

    public static function is_array_associative($array)
    {
        return count(array_filter(array_keys($array), 'is_string')) > 0;
    }

    public static function is_array_sequential($array)
    {
        return !Util::is_array_associative($array);
    }
}

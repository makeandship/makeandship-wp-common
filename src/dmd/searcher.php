<?php

namespace makeandship\common\dmd;

use makeandship\common\dmd\DmdSettingsManager;
use makeandship\common\dmd\transformer\SuggestionTransformer;
use \Elastica\Client;
use \Elastica\Request;

/**
 * Run searches against the backing Elasticsearch server configured in the plugin settings
 **/
class Searcher
{
    private $client;

    public function __construct()
    {
        $settings_manager = new DmdSettingsManager();
        $client_settings  = $settings_manager->get_client_settings();

        $this->client = new Client($client_settings);
    }

    /**
     * Execute an elastic search query.  Use a <code>QueryBuilder</code> to generate valid queries
     *
     * @param array an elastic search query
     * @return results object
     *
     * @see QueryBuilder
     */
    public function search($args)
    {
        Util::log("Searcher#search", "enter");
        $results = array();

        $args = Util::apply_filters('prepare_query', $args);

        Util::log("Searcher#search: args", json_encode($args));

        try {
            $index = $this->get_index();

            $path = $index->getName() . '/_search';

            $response = $this->client->request($path, Request::GET, $args);
            if ($response) {
                $data = $response->getData();

                $transformer = new SuggestionTransformer();
                $results     = $transformer->transform($data, array());
            }

            Util::log("Searcher#search", "exit");

            return $results;
        } catch (\Exception $ex) {
            error_log($ex);

            Util::log("Searcher#search: exception", $ex);

            Util::log("Searcher#search: exception", "exit");

            return null;
        }
    }

    /**
     * Get a document using the document id
     *
     * get_document_by_id
     *
     * @param id
     * @return document
     */
    public function get_document_by_id($id)
    {
        Util::log("Searcher#get_document_by_id", "enter");
        $document = null;

        Util::log("Searcher#get_document_by_id: id: ", $id);

        try {
            $index = $this->get_index();

            $response = $index->getDocument($id);

            if ($response) {
                $document = $response->getData();
            }

            Util::log("Searcher#get_document_by_id", "exit");

            return $document;
        } catch (\Exception $ex) {
            Util::log("Searcher#get_document_by_id: exception: ", $ex);

            Util::log("Searcher#get_document_by_id: exception", "exit");

            return null;
        }
    }

    private function get_index()
    {
        $index_name = Constants::INDEX_NAME;
        return $this->client->getIndex($index_name);
    }
}

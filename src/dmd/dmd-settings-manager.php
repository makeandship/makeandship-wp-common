<?php

namespace makeandship\common\dmd;

use makeandship\common\dmd\Constants;
use makeandship\common\settings\SettingsManager;
use makeandship\common\Util;

class DmdSettingsManager extends SettingsManager
{
    public function __construct()
    {
        $this->initialize();
    }

    protected function initialize()
    {
        $this->get_settings(true);
    }

    /**
     * Get the current configuration.  Configuration values
     * are cached.  Use the $fresh parameter to get an updated
     * set
     *
     * @param $fresh - true to get updated values
     * @return array of settings
     */
    public function get_settings($fresh = false)
    {
        if (!isset($this->settings) || $fresh) {
            $this->settings = array();

            $scheme   = getenv(Constants::ENV_DMD_ES_SCHEME);
            $host     = getenv(Constants::ENV_DMD_ES_HOST);
            $port     = getenv(Constants::ENV_DMD_ES_PORT);
            $index    = getenv(Constants::ENV_DMD_ES_INDEX);
            $username = getenv(Constants::ENV_DMD_ES_USERNAME);
            $password = getenv(Constants::ENV_DMD_ES_PASSWORD);

            $this->settings[Constants::OPTION_SERVER]                = $scheme . '://' . $host . ':' . $port . '/';
            $this->settings[Constants::OPTION_INDEX_NAME]            = $index;
            $this->settings[Constants::OPTION_READ_TIMEOUT]          = Constants::DEFAULT_READ_TIMEOUT;
            $this->settings[Constants::OPTION_USERNAME]              = $username;
            $this->settings[Constants::OPTION_PASSWORD]              = $password;
            $this->settings[Constants::OPTION_ELASTICSEARCH_VERSION] = $this->get_elasticseach_version();
        }

        return $this->settings;
    }

    public function get($name)
    {
        $settings = $this->get_settings();

        return Util::safely_get_attribute($settings, $name);
    }

    public function set($name, $value)
    {
        if ($this->valid_setting($name)) {
            $this->set_option($name, $value);

            if ($this->settings) {
                $this->settings[$name] = $value;
            }
        }
    }

    private function valid_setting($name)
    {
        if ($name) {
            if (in_array($name, [
                Constants::OPTION_SERVER,
                Constants::OPTION_INDEX_NAME,
                Constants::OPTION_READ_TIMEOUT,
                Constants::OPTION_USERNAME,
                Constants::OPTION_PASSWORD,
                Constants::OPTION_ELASTICSEARCH_VERSION,
            ])) {
                return true;
            }
        }

        return false;
    }

    /**
     * Return settings to connect to the configured elasticsearch instance
     *
     * @return array with options set
     */
    public function get_client_settings()
    {
        $settings = array();

        $settings[Constants::SETTING_URL] = $this->get(Constants::OPTION_SERVER);

        $username = $this->get(Constants::OPTION_USERNAME);
        if ($username) {
            $settings[Constants::SETTING_USERNAME] = $username;
        }

        $password = $this->get(Constants::OPTION_PASSWORD);
        if ($password) {
            $settings[Constants::SETTING_PASSWORD] = $password;
        }

        return $settings;
    }

    private function get_elasticseach_version()
    {
        $client_settings = $this->get_client_settings();
        $client          = new Client($client_settings);
        return $client->getVersion();
    }
}

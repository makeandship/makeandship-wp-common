<?php

namespace makeandship\common;

class Validation
{
    // can't be instantiated externally
    protected function __construct()
    {
    }
    // no clone
    protected function __clone()
    {
    }

    public static function initialize()
    {
    }

    /**
     * Validate against a character limit
     */
    public static function words_valid($valid, $value, $field, $limit)
    {
        if (!$valid) {
            return $valid;
        }
        if (is_admin()) {
            $count = str_word_count($value);
            if ($count > $limit) {
                $valid = $field['label'] . ' exceeds ' . $limit . ' words (' . $count . ' words)';
            }
        }

        return $valid;
    }

    public static function words_valid_100($valid, $value, $field, $input)
    {
        return Validation::words_valid($valid, $value, $field, 100);
    }

    public static function words_valid_200($valid, $value, $field, $input)
    {
        return Validation::words_valid($valid, $value, $field, 200);
    }

    public static function words_valid_300($valid, $value, $field, $input)
    {
        return Validation::words_valid($valid, $value, $field, 300);
    }

}

Validation::initialize();
